tracker_types = {
    'gt',...%0
    'esm',...%1
    'ic',...%2
    'nnic',...%3
    'pf',...%4
    'pw',...%5
    'ppw'...%6
    };
filter_types = {
    'none',...%0
    'gauss',...%1
    'box',...%2
    'norm_box',...%3
    'bilateral',...%4
    'median',...%5
    'gabor',...%6
    'sobel',...%7
    'scharr',...%8
    'LoG',...%9
    'DoG',...%10
    'laplacian',...%11
    'canny'%12
    };
inc_types = {'fc',...%0
    'ic',...%1
    'fa',...%2
    'ia'...%3
    };
pw_opt_types = {
    'pre',...%0
    'post',...%1
    'ind'%2
    };
grid_types = {
    'trans',...%0
    'rs',...%1
    'shear',...%2
    'proj',...%3
    'rtx',...%4
    'rty',...%5
    'stx',...%6
    'sty',...%7
    'trans2'%8
    };
appearance_models = {
    'ssd',...%0
    'scv',...%1
    'ncc'...%2
    'mi'...%3
    'ccre',...%4
    'hssd',...%5
    'jht',...%6
    'mi_old',...%7
    'ncc2',...%8
    'scv2',...%9
    'mi2',...%10
    'mssd',...%11
    'bmssd',...%12
    'bmi',...%13
    'crv',...%14
    'fkld',...%15
    'ikld',...%16
    'mkld',...%17
    'chis',...%18
    'ssim',...%19
    'fmaps'...%20    
    };
sequences_tmt = {
    'nl_bookI_s3',...%0
    'nl_bookII_s3',...%1
    'nl_bookIII_s3',...%2
    'nl_cereal_s3',...%3
    'nl_juice_s3',...%4
    'nl_mugI_s3',...%5
    'nl_mugII_s3',...%6
    'nl_mugIII_s3',...%7
    'nl_bookI_s4',...%8
    'nl_bookII_s4',...%9
    'nl_bookIII_s4',...%10
    'nl_cereal_s4',...%11
    'nl_juice_s4',...%12
    'nl_mugI_s4',...%13
    'nl_mugII_s4',...%14
    'nl_mugIII_s4',...%15
    'nl_bus',...%16
    'nl_highlighting',...%17
    'nl_letter',...%18
    'nl_newspaper',...%19
    'nl_bookI_s1',...%20
    'nl_bookII_s1',...%21
    'nl_bookIII_s1',...%22
    'nl_cereal_s1',...%23
    'nl_juice_s1',...%24
    'nl_mugI_s1',...%25
    'nl_mugII_s1',...%26
    'nl_mugIII_s1',...%27
    'nl_bookI_s2',...%28
    'nl_bookII_s2',...%29
    'nl_bookIII_s2',...%30
    'nl_cereal_s2',...%31
    'nl_juice_s2',...%32
    'nl_mugI_s2',...%33
    'nl_mugII_s2',...%34
    'nl_mugIII_s2',...%35
    'nl_bookI_s5',...%36
    'nl_bookII_s5',...%37
    'nl_bookIII_s5',...%38
    'nl_cereal_s5',...%39
    'nl_juice_s5',...%40
    'nl_mugI_s5',...%41
    'nl_mugII_s5',...%42
    'nl_mugIII_s5',...%43
    'nl_bookI_si',...%44
    'nl_bookII_si',...%45
    'nl_cereal_si',...%46
    'nl_juice_si',...%47
    'nl_mugI_si',...%48
    'nl_mugIII_si',...%49
    'dl_bookI_s3',...%50
    'dl_bookII_s3',...%51
    'dl_bookIII_s3',...%52
    'dl_cereal_s3',...%53
    'dl_juice_s3',...%54
    'dl_mugI_s3',...%55
    'dl_mugII_s3',...%56
    'dl_mugIII_s3',...%57
    'dl_bookI_s4',...%58
    'dl_bookII_s4',...%59
    'dl_bookIII_s4',...%60
    'dl_cereal_s4',...%61
    'dl_juice_s4',...%62
    'dl_mugI_s4',...%63
    'dl_mugII_s4',...%64
    'dl_mugIII_s4',...%65
    'dl_bus',...%66
    'dl_highlighting',...%67
    'dl_letter',...%68
    'dl_newspaper',...%69
    'dl_bookI_s1',...%70
    'dl_bookII_s1',...%71
    'dl_bookIII_s1',...%72
    'dl_cereal_s1',...%73
    'dl_juice_s1',...%74
    'dl_mugI_s1',...%75
    'dl_mugII_s1',...%76
    'dl_mugIII_s1',...%77
    'dl_bookI_s2',...%78
    'dl_bookII_s2',...%79
    'dl_bookIII_s2',...%80
    'dl_cereal_s2',...%81
    'dl_juice_s2',...%82
    'dl_mugI_s2',...%83
    'dl_mugII_s2',...%84
    'dl_mugIII_s2',...%85
    'dl_bookI_s5',...%86
    'dl_bookII_s5',...%87
    'dl_bookIII_s5',...%88
    'dl_cereal_s5',...%89
    'dl_juice_s5',...%90
    'dl_mugI_s5',...%91
    'dl_mugIII_s5',...%92
    'dl_bookII_si',...%93
    'dl_cereal_si',...%94
    'dl_juice_si',...%95
    'dl_mugI_si',...%96
    'dl_mugIII_si',...%97
    'dl_mugII_si',...98
    'dl_mugII_s5',...99
    'nl_mugII_si',...100
    'robot_bookI',...101
    'robot_bookII',...102
    'robot_bookIII',...103
    'robot_cereal',...104
    'robot_juice',...105
    'robot_mugI',...106
    'robot_mugII',...107
    'robot_mugIII',...108
    };
sequences_ucsb={
    'bricks_dynamic_lighting',...%0
    'bricks_motion1',...%1
    'bricks_motion2',...%2
    'bricks_motion3',...%3
    'bricks_motion4',...%4
    'bricks_motion5',...%5
    'bricks_motion6',...%6
    'bricks_motion7',...%7
    'bricks_motion8',...%8
    'bricks_motion9',...%9
    'bricks_panning',...%10
    'bricks_perspective',...%11
    'bricks_rotation',...%12
    'bricks_static_lighting',...%13
    'bricks_unconstrained',...%14
    'bricks_zoom',...%15
    'building_dynamic_lighting',...%16
    'building_motion1',...%17
    'building_motion2',...%18
    'building_motion3',...%19
    'building_motion4',...%20
    'building_motion5',...%21
    'building_motion6',...%22
    'building_motion7',...%23
    'building_motion8',...%24
    'building_motion9',...%25
    'building_panning',...%26
    'building_perspective',...%27
    'building_rotation',...%28
    'building_static_lighting',...%29
    'building_unconstrained',...%30
    'building_zoom',...%31
    'mission_dynamic_lighting',...%32
    'mission_motion1',...%33
    'mission_motion2',...%34
    'mission_motion3',...%35
    'mission_motion4',...%36
    'mission_motion5',...%37
    'mission_motion6',...%38
    'mission_motion7',...%39
    'mission_motion8',...%40
    'mission_motion9',...%41
    'mission_panning',...%42
    'mission_perspective',...%43
    'mission_rotation',...%44
    'mission_static_lighting',...%45
    'mission_unconstrained',...%46
    'mission_zoom',...%47
    'paris_dynamic_lighting',...%48
    'paris_motion1',...%49
    'paris_motion2',...%50
    'paris_motion3',...%51
    'paris_motion4',...%52
    'paris_motion5',...%53
    'paris_motion6',...%54
    'paris_motion7',...%55
    'paris_motion8',...%56
    'paris_motion9',...%57
    'paris_panning',...%58
    'paris_perspective',...%59
    'paris_rotation',...%60
    'paris_static_lighting',...%61
    'paris_unconstrained',...%62
    'paris_zoom',...%63
    'sunset_dynamic_lighting',...%64
    'sunset_motion1',...%65
    'sunset_motion2',...%66
    'sunset_motion3',...%67
    'sunset_motion4',...%68
    'sunset_motion5',...%69
    'sunset_motion6',...%70
    'sunset_motion7',...%71
    'sunset_motion8',...%72
    'sunset_motion9',...%73
    'sunset_panning',...%74
    'sunset_perspective',...%75
    'sunset_rotation',...%76
    'sunset_static_lighting',...%77
    'sunset_unconstrained',...%78
    'sunset_zoom',...%79
    'wood_dynamic_lighting',...%80
    'wood_motion1',...%81
    'wood_motion2',...%82
    'wood_motion3',...%83
    'wood_motion4',...%84
    'wood_motion5',...%85
    'wood_motion6',...%86
    'wood_motion7',...%87
    'wood_motion8',...%88
    'wood_motion9',...%89
    'wood_panning',...%90
    'wood_perspective',...%91
    'wood_rotation',...%92
    'wood_static_lighting',...%93
    'wood_unconstrained',...%94
    'wood_zoom'%95
    };


sequences_lintrack = {
    'mouse_pad',...%0
    'phone',...%1
    'towel'%2
    };

sequences_pami = {
    'acronis',...%0
    'bass',...%1
    'bear',...%2
    'book1',...%3
    'book2',...%4
    'book3',...%5
    'book4',...%6
    'box',...%7
    'cat_cylinder',...%8
    'cat_mask',...%9
    'cat_plane',...%10
    'compact_disc',...%11
    'cube',...%12
    'deformable',...%13
    'deformable_2',...%14
    'mascot',...%15
    'sylvester',...%16
    'tea',...%17
    'board_robot',...%18
    'board_robot_2',...%19
    'box_robot',...%20
    'cup_on_table',...%21
    'juice',...%22
    'lemming',...%23
    'liquor',...%24
    'dft_atlas_still',...%25
    'dft_atlas_moving',...%26
    'dft_still',...%27
    'dft_moving',...%28
    'omni_obelix',...%29
    'omni_magazine',...%30
    'chess_board_1',...%31
    'chess_board_2',...%32
    'chess_board_3',...%33
    'chess_board_4'%34
    };

sequences_cmt = {
    'board_robot',...%0
    'box_robot',...%1
    'cup_on_table',...%2
    'juice',...%3
    'lemming',...%4
    'liquor',...%5
    'sylvester',...%6
    'ball',...%7
    'car',...%8
    'car_2',...%9
    'carchase',...%10
    'dog1',...%11
    'gym',...%12
    'jumping',...%13
    'mountain_bike',...%14
    'person',...%15
    'person_crossing',...%16
    'person_partially_occluded',...%17
    'singer',...%18
    'track_running'%19
    };

sequences_metaio = {
    'bump_angle',...%0
    'bump_fast_close',...%1
    'bump_fast_far',...%2
    'bump_illumination',...%3
    'bump_range',...%4
    'grass_angle',...%5
    'grass_fast_close',...%6
    'grass_fast_far',...%7
    'grass_illumination',...%8
    'grass_range',...%9
    'isetta_angle',...%10
    'isetta_fast_close',...%11
    'isetta_fast_far',...%12
    'isetta_illumination',...%13
    'isetta_range',...%14
    'lucent_angle',...%15
    'lucent_fast_close',...%16
    'lucent_fast_far',...%17
    'lucent_illumination',...%18
    'lucent_range',...%19
    'macMini_angle',...%20
    'macMini_fast_close',...%21
    'macMini_fast_far',...%22
    'macMini_illumination',...%23
    'macMini_range',...%24
    'philadelphia_angle',...%25
    'philadelphia_fast_close',...%26
    'philadelphia_fast_far',...%27
    'philadelphia_illumination',...%28
    'philadelphia_range',...%29
    'stop_angle',...%30
    'stop_fast_close',...%31
    'stop_fast_far',...%32
    'stop_illumination',...%33
    'stop_range',...%34
    'wall_angle',...%35
    'wall_fast_close',...%36
    'wall_fast_far',...%37
    'wall_illumination',...%38
    'wall_range'%39
    };
sequences_synthetic = {
    'line_rot2',...%0
    'line',...%1
    'line_rot'%2
    };

sequences_vivid = {
    'pktest03',...%0
    'egtest01',...%1
    'egtest02',...%2
    'egtest03',...%3
    'egtest04',...%4
    'egtest05',...%5
    'pktest01',...%6
    'pktest02',...%7
    'redteam'%8
    };

sequences_vot = {
    'woman',...%0
    'ball',...%1
    'basketball',...%2
    'bicycle',...%3
    'bolt',...%4
    'car',...%5
    'david',...%6
    'diving',...%7
    'drunk',...%8
    'fernando',...%9
    'fish1',...%10
    'fish2',...%11
    'gymnastics',...%12
    'hand1',...%13
    'hand2',...%14
    'jogging',...%15
    'motocross',...%16
    'polarbear',...%17
    'skating',...%18
    'sphere',...%19
    'sunshade',...%20
    'surfing',...%21
    'torus',...%22
    'trellis',...%23
    'tunnel'%24
    };

sequences_vtb = {
    'Basketball',...%0
    'Biker',...%1
    'Bird1',...%2
    'Bird2',...%3
    'BlurBody',...%4
    'BlurCar1',...%5
    'BlurCar2',...%6
    'BlurCar3',...%7
    'BlurCar4',...%8
    'BlurFace',...%9
    'BlurOwl',...%10
    'Board',...%11
    'Bolt',...%12
    'Bolt2',...%13
    'Box',...%14
    'Boy',...%15
    'Car1',...%16
    'Car2',...%17
    'Car4',...%18
    'Car24',...%19
    'CarDark',...%20
    'CarScale',...%21
    'ClifBar',...%22
    'Coke',...%23
    'Couple',...%24
    'Coupon',...%25
    'Crossing',...%26
    'Crowds',...%27
    'Dancer',...%28
    'Dancer2',...%29
    'David',...%30
    'David2',...%31
    'David3',...%32
    'Deer',...%33
    'Diving',...%34
    'Dog',...%35
    'Dog1',...%36
    'Doll',...%37
    'DragonBaby',...%38
    'Dudek',...%39
    'FaceOcc1',...%40
    'FaceOcc2',...%41
    'Fish',...%42
    'FleetFace',...%43
    'Football',...%44
    'Football1',...%45
    'Freeman1',...%46
    'Freeman3',...%47
    'Freeman4',...%48
    'Girl',...%49
    'Girl2',...%50
    'Gym',...%51
    'Human2',...%52
    'Human3',...%53
    'Human4',...%54
    'Human5',...%55
    'Human6',...%56
    'Human7',...%57
    'Human8',...%58
    'Human9',...%59
    'Ironman',...%60
    'Jogging',...%61
    'Jogging_2',...%62
    'Jump',...%63
    'Jumping',...%64
    'KiteSurf',...%65
    'Lemming',...%66
    'Liquor',...%67
    'Man',...%68
    'Matrix',...%69
    'Mhyang',...%70
    'MotorRolling',...%71
    'MountainBike',...%72
    'Panda',...%73
    'RedTeam',...%74
    'Rubik',...%75
    'Shaking',...%76
    'Singer1',...%77
    'Singer2',...%78
    'Skater',...%79
    'Skater2',...%80
    'Skating1',...%81
    'Skating2',...%82
    'Skating2_2',...%83
    'Skiing',...%84
    'Soccer',...%85
    'Subway',...%86
    'Surfer',...%87
    'Suv',...%88
    'Sylvester',...%89
    'Tiger1',...%90
    'Tiger2',...%91
    'Toy',...%92
    'Trans',...%93
    'Trellis',...%94
    'Twinnings',...%95
    'Vase',...%96
    'Walking',...%97
    'Walking2',...%98
    'Woman'%99
    };

sequences_malis = {
    'deformable',...%0
    'bear_1',...%1
    'book_1',...%2
    'box',...%3
    'cat_cylinder',...%4
    'cat_plane'%5
    };
sequences_trakmark = {
    'CV00_00',...%0
    'CV00_01',...%1
    'CV00_02',...%2
    'CV01_00',...%3
    'FS00_00',...%4
    'FS00_01',...%5
    'FS00_02',...%6
    'FS00_03',...%7
    'FS00_04',...%8
    'FS00_05',...%9
    'FS00_06',...%10
    'FS01_00',...%11
    'FS01_01',...%12
    'FS01_02',...%13
    'FS01_03',...%14
    'JR00_00',...%15
    'JR00_01',...%16
    'NC00_00',...%17
    'NC01_00',...%18
    'NH00_00',...%19
    'NH00_01'%20
    };

sequences_live = {
    'usb_cam',...%0
    'firewire_cam'...%1
    };

challenges = {
    'angle',...%0
    'fast_close',...%1
    'fast_far',...%2
    'range',...%3
    'illumination'...%4
    };


sequences={
    sequences_tmt,...
    sequences_ucsb,...
    sequences_lintrack,...
    sequences_pami,...
    sequences_cmt,...
    sequences_metaio,...
    sequences_vivid,...
    sequences_vot,...
    sequences_vtb,...
    sequences_malis,...
    sequences_trakmark,...
    sequences_synthetic,...
    sequences_live
    };

actors = {
    'TMT',...%0
    'UCSB',...%1
    'LinTrack',...%2
    'PAMI',...%3
    'CMT',...%4
    'METAIO',...%5
    'VIVID',...%6
    'VOT',...%7
    'VTB',...%8
    'MALIS',...%9
    'TrakMark',...%10
    'Synthetic',...%11
    'Live'%12
    };


tmt_idx_all=1:length(sequences_tmt);
tmt_idx_nl=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, 'nl_')));
tmt_idx_dl=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, 'dl_')));

tmt_idx_s1=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_s1')));
tmt_idx_s2=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_s2')));
tmt_idx_s3=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_s3')));
tmt_idx_s4=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_s4')));
tmt_idx_s5=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_s5')));
tmt_idx_si=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_si')));

tmt_idx_bookI=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_bookI')));
tmt_idx_bookII=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_bookII')));
tmt_idx_bookIII=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_bookIII')));
tmt_idx_cereal=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_cereal')));
tmt_idx_juice=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_juice')));
tmt_idx_mugI=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_mugI')));
tmt_idx_mugII=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_mugII')));
tmt_idx_mugIII=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_mugIII')));

tmt_idx_bus=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_bus')));
tmt_idx_highlighting=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_highlighting')));
tmt_idx_newspaper=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_newspaper')));
tmt_idx_letter=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, '_letter')));
tmt_idx_composite=[tmt_idx_bus tmt_idx_highlighting tmt_idx_newspaper tmt_idx_letter];
tmt_idx_robot=tmt_idx_all(~cellfun('isempty',strfind(sequences_tmt, 'robot_')));


tmt_idxs={
    tmt_idx_all,...
    tmt_idx_nl,...
    tmt_idx_dl,...
    tmt_idx_s1,...
    tmt_idx_s2,...
    tmt_idx_s3,...
    tmt_idx_s4,...
    tmt_idx_s5,...
    tmt_idx_si,...
    tmt_idx_bookI,...
    tmt_idx_bookII,...
    tmt_idx_bookIII,...
    tmt_idx_cereal,...
    tmt_idx_juice,...
    tmt_idx_mugI,...
    tmt_idx_mugII,...
    tmt_idx_mugIII,...
    tmt_idx_bus,...
    tmt_idx_highlighting,...
    tmt_idx_newspaper,...
    tmt_idx_letter,...
    tmt_idx_composite,...
    tmt_idx_robot
    };

tmt_idx_types={
    'all',...%0
    'nl',...%1
    'dl',...%2
    's1',...%3
    's2',...%4
    's3',...%5
    's4',...%6
    's5',...%7
    'si',...%8
    'bookI',...%9
    'bookII',...%10
    'bookIII',...%11
    'cereal',...%12
    'juice',...%13
    'mugI',...%14
    'mugII',...%15
    'mugIII',...%16
    'bus',...%17
    'highlighting',...%18
    'newspaper',...%19
    'letter',...%20
    'composite',...%21
    'robot'%22
    };

ucsb_idx_all=1:length(sequences_ucsb);
ucsb_idx_bricks=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, 'bricks_')));
ucsb_idx_building=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, 'building_')));
ucsb_idx_mission=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, 'mission_')));
ucsb_idx_sunset=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, 'sunset_')));
ucsb_idx_paris=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, 'paris_')));
ucsb_idx_wood=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, 'wood_')));

ucsb_idx_dynamic_lighting=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_dynamic_lighting')));
ucsb_idx_motion1=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_motion1')));
ucsb_idx_motion2=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_motion2')));
ucsb_idx_motion3=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_motion3')));
ucsb_idx_motion4=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_motion4')));
ucsb_idx_motion5=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_motion5')));
ucsb_idx_motion6=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_motion6')));
ucsb_idx_motion7=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_motion7')));
ucsb_idx_motion8=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_motion8')));
ucsb_idx_motion9=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_motion9')));
ucsb_idx_unconstrained=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_unconstrained')));
ucsb_idx_zoom=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_zoom')));
ucsb_idx_rotation=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_rotation')));
ucsb_idx_static_lighting=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_static_lighting')));
ucsb_idx_perspective=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_perspective')));
ucsb_idx_panning=ucsb_idx_all(~cellfun('isempty',strfind(sequences_ucsb, '_panning')));

ucsb_idxs={
    ucsb_idx_all,...
    ucsb_idx_bricks,...
    ucsb_idx_building,...
    ucsb_idx_mission,...
    ucsb_idx_sunset,...
    ucsb_idx_paris,...
    ucsb_idx_wood,...
    ucsb_idx_dynamic_lighting,...
    ucsb_idx_motion1,...
    ucsb_idx_motion2,...
    ucsb_idx_motion3,...
    ucsb_idx_motion4,...
    ucsb_idx_motion5,...
    ucsb_idx_motion6,...
    ucsb_idx_motion7,...
    ucsb_idx_motion8,...
    ucsb_idx_motion9,...
    ucsb_idx_unconstrained,...
    ucsb_idx_zoom,...
    ucsb_idx_rotation,...
    ucsb_idx_static_lighting,...
    ucsb_idx_perspective,...
    ucsb_idx_panning
    };

ucsb_idx_types={
    'all',...%0
    'bricks',...%1
    'building',...%2
    'mission',...%3
    'sunset',...%4
    'paris',...%5
    'wood',...%6
    'dynamic_lighting',...%7
    'motion1',...%8
    'motion2',...%9
    'motion3',...%10
    'motion4',...%11
    'motion5',...%12
    'motion6',...%13
    'motion7',...%14
    'motion8',...%15
    'motion9',...%16
    'unconstrained',...%17
    'zoom',...%18
    'rotation',...%19
    'static_lighting',...%20
    'perspective',...%21
    'panning'%22
    };

lintrack_idx_all=1:3;
pami_idx_all=1:25;
cmt_idx_all=1:20;
metaio_idx_all=1:40;
vivid_idx_all=1:9;
vot_idx_all=1:25;
vtb_idx_all=1:100;
malis_idx_all=1:6;
trakmark_idx_all=1:21;
lintrack_idxs={
    lintrack_idx_all,...
    [0],...
    [1],...
    [2]
    };
lintrack_idx_types={
    'all',...%0
    'mouse_pad',...%1
    'phone',...%2
    'towel'%3
    };
pami_idxs={
    pami_idx_all
    };
pami_idx_types={
    'all'
    };
cmt_idxs={
    cmt_idx_all
    };
cmt_idx_types={
    'all'
    };
metaio_idxs={
    metaio_idx_all
    };
metaio_idx_types={
    'all'
    };
vivid_idxs={
    vivid_idx_all
    };
vivid_idx_types={
    'all'
    };
vot_idxs={
    vot_idx_all
    };
vot_idx_types={
    'all'
    };
vtb_idxs={
    vtb_idx_all
    };
vtb_idx_types={
    'all'
    };
malis_idxs={
    malis_idx_all
    };
malis_idx_types={
    'all'
    };
trakmark_idxs={
    trakmark_idx_all
    };
trakmark_idx_types={
    'all'
    };

actor_idxs={
    tmt_idxs,...
    ucsb_idxs,...
    lintrack_idxs,...
    pami_idxs,...
    cmt_idxs,...    
    metaio_idxs,...
    vivid_idxs,...
    vot_idxs,...
    vtb_idxs,...
    malis_idxs,...
    trakmark_idxs
    };
actor_idx_types={
    tmt_idx_types,...
    ucsb_idx_types,...
    lintrack_idx_types,...
    pami_idx_types,...
    cmt_idx_types,...	
    metaio_idx_types,...
    vivid_idx_types,...
    vot_idx_types,...
    vtb_idx_types,...
    malis_idx_types,...
    trakmark_idx_types
    };

db_root_dir='../../Datasets';
tmt_n_frames=importdata(sprintf('%s/TMT/n_frames.txt', db_root_dir));
ucsb_n_frames=importdata(sprintf('%s/UCSB/n_frames.txt', db_root_dir));
lintrack_n_frames=importdata(sprintf('%s/LinTrack/n_frames.txt', db_root_dir));
pami_n_frames=importdata(sprintf('%s/PAMI/n_frames.txt', db_root_dir));
cmt_n_frames=importdata(sprintf('%s/CMT/n_frames.txt', db_root_dir));
metaio_n_frames=importdata(sprintf('%s/METAIO/n_frames.txt', db_root_dir));
vivid_n_frames=importdata(sprintf('%s/VIVID/n_frames.txt', db_root_dir));
vot_n_frames=importdata(sprintf('%s/VOT/n_frames.txt', db_root_dir));
vtb_n_frames=importdata(sprintf('%s/VTB/n_frames.txt', db_root_dir));
malis_n_frames=0;
trakmark_n_frames=0;
actor_n_frames={
    tmt_n_frames,...
    ucsb_n_frames,...
    lintrack_n_frames,...
    pami_n_frames,...
    cmt_n_frames,...
    metaio_n_frames,...
    vivid_n_frames,...
    vot_n_frames,...
    vtb_n_frames,...
    malis_n_frames,...
    trakmark_n_frames
    };
