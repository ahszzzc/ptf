root_dir='.';
read_from_list = 0;
if read_from_list
    file_list=importdata(sprintf('%s/list.txt',root_dir))
else
    file_list={''}
end
format='wmv';
for file_id=1:length(file_list)
    filename=file_list{file_id};
    image_dir=sprintf('%s/%s',root_dir,filename);
    if(~exist(image_dir, 'dir'))
        mkdir(image_dir);
    end
    file_path=sprintf('%s/%s.%s',root_dir,filename,format)
    video = VideoReader(file_path);
    fprintf('Processing file: %s\n',file_path);
    frame_id=0;
    while hasFrame(video)
        frame_id=frame_id+1;
        fprintf('\tDone processing %d frames\n',frame_id);
        img = readFrame(video);
        imwrite(img, sprintf('%s/frame%05d.jpg',image_dir, frame_id), 'Quality', 100);
    end
end

