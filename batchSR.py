import os
import zipfile
import sys
import subprocess

if __name__ == '__main__':

    use_arch = 1
    arch_root_dir = './C++/MTF/log/archives'
    arch_name = 'resf_dsst_4_subseq10_tmt'
    in_arch_path = 'tracking_data'
    gt_root_dir = '../Datasets'
    tracking_root_dir = './C++/MTF/log/tracking_data'
    out_dir = './C++/MTF/log/success_rates'

    actor_ids = [0]
    opt_gt_ssms = ['4']

    enable_subseq = 0
    n_subseq = 10

    reinit_from_gt = 1
    reinit_frame_skip = 5
    reinit_err_thresh = 20

    use_reinit_gt = 0

    list_fname = 'list.txt'
    list_in_arch = 1
    # list_fname = '{:s}/{:s}.txt'.format(arch_root_dir, arch_name)

    arg_id = 1
    if len(sys.argv) > arg_id:
        arch_name = sys.argv[arg_id]
        arg_id += 1
    if len(sys.argv) > arg_id:
        reinit_from_gt = int(sys.argv[arg_id])
        arg_id += 1
    if len(sys.argv) > arg_id:
        reinit_frame_skip = int(sys.argv[arg_id])
        arg_id += 1
    if len(sys.argv) > arg_id:
        reinit_err_thresh = float(sys.argv[arg_id])
        arg_id += 1
    if len(sys.argv) > arg_id:
        arch_root_dir = sys.argv[arg_id]
        arg_id += 1
    if len(sys.argv) > arg_id:
        gt_root_dir = sys.argv[arg_id]
        arg_id += 1
    if len(sys.argv) > arg_id:
        tracking_root_dir = sys.argv[arg_id]
        arg_id += 1
    if len(sys.argv) > arg_id:
        out_dir = sys.argv[arg_id]
        arg_id += 1

    # reinit gt only used with reinit tests
    # use_reinit_gt = use_reinit_gt or reinit_from_gt

    # sub sequence tests only run without reinitialization
    enable_subseq = enable_subseq and not reinit_from_gt

    arch_path = '{:s}/{:s}.zip'.format(arch_root_dir, arch_name)
    print 'Reading tracking data from zip archive: {:s}'.format(arch_path)
    arch_fid = zipfile.ZipFile(arch_path, 'r')

    # if not os.path.isfile(list_fname):
    # print 'List file for the batch job does  not exist:\n {:s}'.format(list_fname)

    if list_in_arch:
        file_list = arch_fid.open(list_fname, 'r').readlines()
    else:
        file_list = open('{:s}/{:s}'.format(arch_root_dir, list_fname), 'r').readlines()

    # exclude files corresponding to sub sequence runs if any
    file_list = [file_name for file_name in file_list if '_init_' not in file_name]

    n_files = len(file_list)
    if len(opt_gt_ssms) > 1 and len(opt_gt_ssms) != n_files:
        raise SyntaxError('Incorrect number of optimal GT specifiers given: {:d}'.format(len(opt_gt_ssms)))
    print 'Generating success rates for following {:d} files: \n'.format(n_files), file_list
    line_id = 0
    for line in file_list:
        line = line.rstrip()
        # line = line.rstrip('.txt')
        line = os.path.splitext(line)[0]
        print 'processing line: ', line
        words = line.split('_')
        mtf_sm = words[0]
        mtf_am = words[1]
        mtf_ssm = words[2]
        iiw = words[3]
        if (len(opt_gt_ssms) > 1):
            opt_gt_ssm = opt_gt_ssms[line_id]
        else:
            opt_gt_ssm = opt_gt_ssms[0]
        for actor_id in actor_ids:
            full_command = \
                'python successGeneralFast.py {:d} {:s} {:s} {:s} {:s} {:s} {:s} {:s} {:s} {:s} {:s} {:s} {:d}  {:d} {:d} {:f} {:d} {:d}'.format(
                    actor_id, mtf_sm, mtf_am, mtf_ssm, iiw, arch_name, in_arch_path, arch_root_dir, gt_root_dir,
                    tracking_root_dir,
                    out_dir, opt_gt_ssm, use_reinit_gt, reinit_from_gt, reinit_frame_skip, reinit_err_thresh,
                    enable_subseq, n_subseq)
            print 'running: {:s}'.format(full_command)
            subprocess.check_call(full_command, shell=True)
            # status = os.system(full_command)
            # if not status:
            # s = raw_input('Last command not completed successfully. Continue ?\n')
            # if s == 'n' or s == 'N':
            # sys.exit()
        line_id += 1


