from Misc import col_rgb
from Misc import getParamDict
from Misc import getNormalizedUnitSquarePts
from Misc import drawRegion
from Misc import getPixVals
from Misc import writeCorners
import utility as util

import os
import cv2
import numpy as np

import pdb

# global variables
curr_corners = None
final_location_selected_mouse = True
pts_confirmed = False


def init_template(img):
    '''
        Return the corners of the template

        It calls getTrackingObject2 to show the selected bounding box
        and the corresponding template, so the user can adjust it.
    '''
    annotated_img = img.copy()
    title = 'Select the object to track'
    cv2.namedWindow(title)
    cv2.imshow(title, annotated_img)
    pts = []
    col=col_rgb['red']
    line_thickness=1

    def drawLines(img, hover_pt=None):
        # If no pts yet, show the original image
        if len(pts) == 0:
            cv2.imshow(title, img)
            return
        for i in xrange(len(pts) - 1):
            cv2.line(img, pts[i], pts[i + 1], col, line_thickness)
        if hover_pt is None:
            return
        cv2.line(img, pts[-1], hover_pt, col, line_thickness)
        if len(pts) == 3:
            cv2.line(img, pts[0], hover_pt, col, line_thickness)
        elif len(pts) == 4:
            return
        cv2.imshow(title, img)

    def mouseHandler(event, x, y, flags=None, param=None):
        if len(pts) >= 4:
            return
        if event == cv2.EVENT_LBUTTONDOWN:
            pts.append((x, y))
            temp_img = annotated_img.copy()
            drawLines(temp_img)
        elif event == cv2.EVENT_LBUTTONUP:
            pass
        elif event == cv2.EVENT_RBUTTONDOWN:
            if len(pts) > 0:
                print 'Removing last point'
                del (pts[-1])
            temp_img = annotated_img.copy()
            drawLines(temp_img)
        elif event == cv2.EVENT_RBUTTONUP:
            pass
        elif event == cv2.EVENT_MBUTTONDOWN:
            pass
        elif event == cv2.EVENT_MOUSEMOVE:
            temp_img = annotated_img.copy()
            drawLines(temp_img, (x, y))

    cv2.setMouseCallback(title, mouseHandler)
    while len(pts) < 4:
        key = cv2.waitKey(1)
        if key == 27:
            sys.exit()
    cv2.waitKey(250)
    cv2.destroyWindow(title)
    drawLines(annotated_img, pts[0])
    return pts


def getNearestCorner(pt):
    '''
        Input:
            pt: [x,y]
        Output:
            min_dist_id:
                the id of the corner where the selected pt is closesest to
    '''
    centroid = np.mean(curr_corners, axis=1)
    print 'centroid: ', centroid

    diff_x = centroid[0] - pt[0]
    diff_y = centroid[1] - pt[1]

    min_dist = diff_x * diff_x + diff_y * diff_y
    min_dist_id = -1
    for i in xrange(0, 4):
        diff_x = curr_corners[0, i] - pt[0]
        diff_y = curr_corners[1, i] - pt[1]

        curr_dist = diff_x * diff_x + diff_y * diff_y
        if curr_dist < min_dist:
            min_dist = curr_dist
            min_dist_id = i

    return min_dist_id


def mouseHandler(event, x, y, flags=None, param=None):
    global left_click_location
    global final_location_selected_mouse
    global curr_corners, curr_pt_id
    global corners_changed
    global pts_confirmed

    if event == cv2.EVENT_LBUTTONDOWN:
        left_click_location = [x, y]
        final_location_selected_mouse = not final_location_selected_mouse
        if not final_location_selected_mouse:
            print 'Patch selected for mouse modification'
            curr_pt_id = getNearestCorner(left_click_location)
        else:
            print 'Patch position confirmed'
        return
    elif event == cv2.EVENT_LBUTTONUP:
        final_location_selected_mouse = not final_location_selected_mouse
    elif event == cv2.EVENT_RBUTTONDOWN:
        pts_confirmed = True
    elif event == cv2.EVENT_RBUTTONUP:
        pass
    elif event == cv2.EVENT_MBUTTONDOWN:
        pass
    elif event == cv2.EVENT_MOUSEMOVE:
        if final_location_selected_mouse:
            return
        if curr_pt_id == -1:
            # if close to centroid, shift all corners
            centroid = np.mean(curr_corners, axis=1)
            centroid_disp_x = x - centroid[0]
            centroid_disp_y = y - centroid[1]
            curr_corners[0, :] += centroid_disp_x
            curr_corners[1, :] += centroid_disp_y
        else:
            # shift the corner with the curr_pt_id
            curr_corners[0, curr_pt_id] = x
            curr_corners[1, curr_pt_id] = y
        corners_changed = True


if __name__ == '__main__':
    params_dict = getParamDict()
    actors = params_dict['actors']
    sequences = params_dict['sequences']
    db_root_dir = '/usr/data/Datasets'
    img_name_fmt = 'frame%05d'
    img_name_ext = 'jpg'
    gt_col = col_rgb['green']

    actor_id = 3
    seq_id = 16
    init_frame_id = 0
    resize_factor = 2
    res_x = 150
    res_y = 150

    actor = actors[actor_id]
    sequences = sequences[actor]
    seq_name = sequences[seq_id]

    img_name_fmt = '{:s}.{:s}'.format(img_name_fmt, img_name_ext)
    src_dir = db_root_dir + '/' + actor + '/' + seq_name
    src_fname = src_dir + '/' + img_name_fmt

    fname = '{:s}/frame{:05d}.jpg'.format(src_dir, init_frame_id + 1)
    print 'fname: ', fname
    print 'src_dir', src_dir
    print 'src_fname', src_fname
    init_img = cv2.imread(fname)
    # Get initial corners
    init_corners = init_template(init_img)
    print 'init_corners', init_corners

    # convert to grayscale and rename it
    init_img_gs = cv2.cvtColor(init_img, cv2.cv.CV_BGR2GRAY).astype(np.float64)

    # Refine the points
    init_corners = np.asarray(init_corners).astype(np.float64).T

    n_pts = res_x * res_y
    patch_size_x = res_x * resize_factor
    patch_size_y = res_y * resize_factor

    # First sample res_x by res_y points from unit square
    std_pts, std_corners = getNormalizedUnitSquarePts(res_x, res_y, 0.5) # why 0.5?
    std_pts_hm = util.homogenize(std_pts)
    init_hom_mat = np.mat(util.compute_homography(std_corners, init_corners))
    # sample points of the region
    init_pts = util.dehomogenize(init_hom_mat * std_pts_hm)

    # .mat does not make a copy
    # bilinear interpolation to get the pixel values of all points
    init_pixel_vals = np.mat([util.bilin_interp(init_img_gs, init_pts[0, pt_id], init_pts[1, pt_id])
                            for pt_id in xrange(n_pts)])

    # Visualize the initial patch
    init_patch = np.reshape(init_pixel_vals, (res_y, res_x)).astype(np.uint8)
    init_patch_resized = cv2.resize(init_patch, (patch_size_x, patch_size_y))
    init_patch_win_name = 'Initial Patch'
    cv2.namedWindow(init_patch_win_name)
    cv2.imshow(init_patch_win_name, init_patch_resized)

    # Visualize the initial image
    curr_img = init_img.copy() # make a copy before drawing corners
    drawRegion(curr_img, init_corners, gt_col, 1, annotate_corners=True)
    init_img_win_name = seq_name
    cv2.namedWindow(init_img_win_name)
    cv2.imshow(init_img_win_name, curr_img)

    # Visualize the selected template
    curr_patch_win_name = 'Current Patch'
    cv2.namedWindow(curr_patch_win_name)
    curr_pixel_vals = init_pixel_vals.copy()
    curr_patch_resized = init_patch_resized

    cv2.setMouseCallback(init_img_win_name, mouseHandler)

    corners_changed = False
    curr_corners = init_corners
    curr_pts = init_pts
    curr_img_gs = init_img_gs
    drawn_img = curr_img.copy()

    while not pts_confirmed:
        key = cv2.waitKey(1) % 256

        if key == 27: # 'Esc'
            break

        if corners_changed:
            # print 'current corners: ', curr_corners
            corners_changed = False
            try:
                curr_hom_mat = np.mat(util.compute_homography(std_corners, curr_corners))
                curr_pts = util.dehomogenize(curr_hom_mat * std_pts_hm)
                curr_pixel_vals = getPixVals(curr_pts, curr_img_gs)
                curr_patch = np.reshape(curr_pixel_vals, (res_y, res_x)).astype(np.uint8)
                curr_patch_resized = cv2.resize(curr_patch, (patch_size_x, patch_size_y))
                drawn_img = init_img.copy()
                drawRegion(drawn_img, curr_corners, gt_col, 1, annotate_corners=True)
            except IndexError:
                print 'curr_corners: \n', curr_corners
                print 'curr_hom_mat: \n', curr_hom_mat
                print 'Invalid homography or out of range corners'
                continue
        cv2.imshow(init_img_win_name, drawn_img)
        cv2.imshow(curr_patch_win_name, curr_patch_resized)
    print 'The selected corners are: \n', curr_corners
    gt_corners_fname = db_root_dir + '/' + actor + '/' + seq_name + '.txt'
    if os.path.isfile(gt_corners_fname):
        print 'Ground truth file {:s} exists.'.format(gt_corners_fname)
    ans = raw_input('Save to the ground truth file {:s}? [y/n]'.format(gt_corners_fname))
    if ans.lower() == 'y':
        # save to file
        out_file = open(gt_corners_fname, 'w')
        writeCorners(out_file, curr_corners, frame_id = 1, write_header=True)
        out_file.close()
        print 'Completed'
    else:
        print 'Aborted'
    cv2.destroyWindow(init_img_win_name)
    cv2.destroyWindow(curr_patch_win_name)

