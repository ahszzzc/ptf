from Misc import getParamDict
from Misc import readDistGridParams
from Misc import readReinitGT
from Misc import readGT

import numpy as np
import math
import sys
import os
import zipfile


def MCDError(tracker_path_orig, gt_path, _use_cle=0, _arch_fid=None, _reinit_from_gt=0,
             _reinit_frame_skip=5, _use_reinit_gt=0, start_ids=None, overflow_err=1e3):
    print 'Reading ground truth from: {:s}...'.format(gt_path)
    if _use_reinit_gt:
        n_gt_frames, gt_data = readReinitGT(gt_path, 0)
    else:
        n_gt_frames, gt_data = readGT(gt_path)

    if n_gt_frames is None or gt_data is None:
        print "Ground truth could not be read successfully"
        return None, None

    if start_ids is None:
        start_ids = [0]

    mcd_err = []
    failure_count = 0
    for start_id in start_ids:
        if start_id == 0:
            tracker_path = tracker_path_orig
        else:
            tracker_path = tracker_path_orig.replace('.txt', '_init_{:d}.txt'.format(start_id))
        print 'Reading tracking data for start_id {:d} from: {:s}...'.format(start_id, tracker_path)
        if _arch_fid is not None:
            tracking_data = _arch_fid.open(tracker_path, 'r').readlines()
        else:
            tracking_data = open(tracker_path, 'r').readlines()
        if len(tracking_data) < 2:
            print 'Tracking data file is invalid.'
            return None, None
        # remove header
        del (tracking_data[0])
        n_lines = len(tracking_data)

        if not _reinit_from_gt and n_lines != n_gt_frames - start_id:
            print "No. of frames in tracking result ({:d}) and the ground truth ({:d}) do not match".format(
                n_lines, n_gt_frames)
            return None, None

        reinit_gt_id = 0
        reinit_start_id = 0
        # ignore the first frame where tracker was initialized
        line_id = 1
        invalid_tracker_state_found = False
        is_initialized = True
        # id of the last frame where tracking failure was detected
        failure_frame_id = -1

        while line_id < n_lines:
            tracking_data_line = tracking_data[line_id].strip().split()
            frame_fname = str(tracking_data_line[0])
            fname_len = len(frame_fname)
            frame_fname_1 = frame_fname[0:5]
            frame_fname_2 = frame_fname[- 4:]
            if frame_fname_1 != 'frame' or frame_fname_2 != '.jpg':
                print 'Invaid formatting on tracking data line {:d}: {:s}'.format(line_id + 1, tracking_data_line)
                print 'frame_fname: {:s} fname_len: {:d} frame_fname_1: {:s} frame_fname_2: {:s}'.format(
                    frame_fname, fname_len, frame_fname_1, frame_fname_2)
                return None, None
            frame_id_str = frame_fname[5:-4]
            frame_num = int(frame_id_str)
            if not _reinit_from_gt and frame_num != start_id + line_id + 1:
                print "Unexpected frame number {:d} found in line {:d} of tracking result for start_id {:d}: {:s}".format(
                    frame_num, line_id + 1, start_id, tracking_data_line)
                return None, None
            if is_initialized:
                # id of the frame in which the tracker is reinitialized
                reinit_start_id = frame_num - 2
                if failure_frame_id >= 0 and reinit_start_id != failure_frame_id + _reinit_frame_skip:
                    print 'Tracker was reinitialized in frame {:d} rather than {:d} where it should have been with {:d} frames being skipped'.format(
                            reinit_start_id + 1, failure_frame_id + _reinit_frame_skip + 1, _reinit_frame_skip
                        )
                    return None, None
                is_initialized = False

            # print 'line_id: {:d} frame_id_str: {:s} frame_num: {:d}'.format(
            # line_id, frame_id_str, frame_num)
            if len(tracking_data_line) != 9:
                if _reinit_from_gt and len(tracking_data_line) == 2 and tracking_data_line[1] == 'tracker_failed':
                    print 'tracking failure detected in frame: {:d} at line {:d}'.format(frame_num, line_id + 1)
                    failure_count += 1
                    failure_frame_id = frame_num - 1
                    # skip the frame where the tracker failed as well as the one where it was reinitialized
                    # whose result will (or should) be in the line following this one
                    line_id += 2
                    is_initialized = True
                    continue
                elif len(tracking_data_line) == 2 and tracking_data_line[1] == 'invalid_tracker_state':
                    if not invalid_tracker_state_found:
                        print 'invalid tracker state detected in frame: {:d} at line {:d}'.format(frame_num,
                                                                                                  line_id + 1)
                        invalid_tracker_state_found = True
                    line_id += 1
                    mcd_err.append(overflow_err)
                    continue
                else:
                    print 'Invalid formatting on line {:d}: {:s}'.format(line_id, tracking_data[line_id])
                    return None, None
            # if is_initialized:frame_num
            # is_initialized = False
            # line_id += 1
            # continue


            if _use_reinit_gt:
                if reinit_gt_id != reinit_start_id:
                    n_gt_frames, gt_data = readReinitGT(gt_path, reinit_start_id)
                    reinit_gt_id = reinit_start_id
                curr_gt = gt_data[frame_num - reinit_start_id - 1]
            else:
                curr_gt = gt_data[frame_num - 1]

            curr_tracking_data = [float(tracking_data_line[1]), float(tracking_data_line[2]),
                                  float(tracking_data_line[3]), float(tracking_data_line[4]),
                                  float(tracking_data_line[5]), float(tracking_data_line[6]),
                                  float(tracking_data_line[7]), float(tracking_data_line[8])]
            # print 'line_id: {:d} gt: {:s}'.format(line_id, gt_line)
            if _use_cle:
                # center location error

                # centroid_tracker_x = (float(tracking_data_line[1]) + float(tracking_data_line[3]) + float(
                # tracking_data_line[5]) + float(tracking_data_line[7])) / 4.0
                # centroid2_x = (float(gt_line[1]) + float(gt_line[3]) + float(gt_line[5]) + float(
                # gt_line[7])) / 4.0

                centroid_tracker_x = (curr_tracking_data[0] + curr_tracking_data[2] + curr_tracking_data[4] +
                                      curr_tracking_data[6]) / 4.0
                centroid_gt_x = (curr_gt[0] + curr_gt[2] + curr_gt[4] + curr_gt[6]) / 4.0

                # centroid_tracker_y = (float(tracking_data_line[2]) + float(tracking_data_line[4]) + float(
                # tracking_data_line[6]) + float(tracking_data_line[8])) / 4.0
                # centroid2_y = (float(gt_line[2]) + float(gt_line[4]) + float(gt_line[6]) + float(
                # gt_line[8])) / 4.0

                centroid_tracker_y = (curr_tracking_data[1] + curr_tracking_data[3] + curr_tracking_data[5] +
                                      curr_tracking_data[7]) / 4.0
                centroid_gt_y = (curr_gt[1] + curr_gt[3] + curr_gt[5] + curr_gt[7]) / 4.0

                err = math.sqrt((centroid_tracker_x - centroid_gt_x) ** 2 + (centroid_tracker_y - centroid_gt_y) ** 2)
                # print 'tracking_data_line: ', tracking_data_line
                # print 'gt_line: ', gt_line
                # print 'centroid1_x: {:15.9f} centroid1_y:  {:15.9f}'.format(centroid1_x, centroid1_y)
                # print 'centroid2_x: {:15.9f} centroid2_y:  {:15.9f}'.format(centroid2_x, centroid2_y)
                # print 'err: {:15.9f}'.format(err)
            else:
                # mean corner distance error
                err = 0
                for corner_id in xrange(4):
                    try:
                        # err += math.sqrt(
                        # (float(tracking_data_line[2 * corner_id + 1]) - float(gt_line[2 * corner_id + 1])) ** 2
                        # + (float(tracking_data_line[2 * corner_id + 2]) - float(gt_line[2 * corner_id + 2])) ** 2
                        # )
                        err += math.sqrt(
                            (curr_tracking_data[2 * corner_id] - curr_gt[2 * corner_id]) ** 2
                            + (curr_tracking_data[2 * corner_id + 1] - curr_gt[2 * corner_id + 1]) ** 2
                        )
                    except OverflowError:
                        err += overflow_err
                        continue
                err /= 4.0
                # for corner_id in range(1, 9):
                # try:
                # err += (float(tracking_data_line[corner_id]) - float(gt_line[corner_id])) ** 2
                # except OverflowError:
                # continue
                # err = math.sqrt(err / 4)
            mcd_err.append(err)
            line_id += 1
        if _reinit_from_gt and n_lines < n_gt_frames - failure_count * (_reinit_frame_skip - 1):
            print "Unexpected no. of frames in reinit tracking result ({:d}) which should be at least {:d}".format(
                n_lines, n_gt_frames - failure_count * (_reinit_frame_skip - 1))
            return None, None
    return mcd_err, failure_count


if __name__ == '__main__':
    use_arch = 1
    arch_root_dir = './C++/MTF/log/archives'
    arch_name = 'ncc__esm__3_4_6__lintrack'
    in_arch_path = 'tracking_data'
    tracking_root_dir = './C++/MTF/log/tracking_data'
    gt_root_dir = '../Datasets'
    out_dir = './C++/MTF/log/success_rates'
    # set this to '0' to use the original ground truth
    mtf_sm = 'ialkC1'
    mtf_am = 'rscv50r30i4u'
    mtf_ssm = '8'
    iiw = 1
    opt_gt_ssm = '0'
    use_reinit_gt = 1
    reinit_from_gt = 0
    reinit_frame_skip = 5
    reinit_err_thresh = 20
    # mtf_am = 'miIN50r10i8b'
    # mtf_am = 'miIN50r10i1i5000s'
    enable_subseq = 0
    n_subseq = 10

    params_dict = getParamDict()
    param_ids = readDistGridParams()

    actors = params_dict['actors']
    sequences = params_dict['sequences']
    mtf_sms = params_dict['mtf_sms']
    mtf_ams = params_dict['mtf_ams']
    mtf_ssms = params_dict['mtf_ssms']

    actor_id = param_ids['actor_id']
    mtf_sm_id = param_ids['mtf_sm_id']
    mtf_am_id = param_ids['mtf_am_id']
    mtf_ssm_id = param_ids['mtf_ssm_id']
    # iiw = param_ids['init_identity_warp']

    arg_id = 1
    if len(sys.argv) > arg_id:
        actor_id = int(sys.argv[arg_id])
        arg_id += 1
    if len(sys.argv) > arg_id:
        mtf_sm = sys.argv[arg_id]
        arg_id += 1
    if len(sys.argv) > arg_id:
        mtf_am = sys.argv[arg_id]
        arg_id += 1
    if len(sys.argv) > arg_id:
        mtf_ssm = sys.argv[arg_id]
        arg_id += 1
    if len(sys.argv) > arg_id:
        iiw = int(sys.argv[arg_id])
        arg_id += 1
    if len(sys.argv) > arg_id:
        arch_name = sys.argv[arg_id]
        arg_id += 1
    if len(sys.argv) > arg_id:
        in_arch_path = sys.argv[arg_id]
        arg_id += 1
    if len(sys.argv) > arg_id:
        arch_root_dir = sys.argv[arg_id]
        arg_id += 1
    if len(sys.argv) > arg_id:
        gt_root_dir = sys.argv[arg_id]
        arg_id += 1
    if len(sys.argv) > arg_id:
        tracking_root_dir = sys.argv[arg_id]
        arg_id += 1
    if len(sys.argv) > arg_id:
        out_dir = sys.argv[arg_id]
        arg_id += 1
    if len(sys.argv) > arg_id:
        opt_gt_ssm = sys.argv[arg_id]
        arg_id += 1
    if len(sys.argv) > arg_id:
        use_reinit_gt = int(sys.argv[arg_id])
        arg_id += 1
    if len(sys.argv) > arg_id:
        reinit_from_gt = int(sys.argv[arg_id])
        arg_id += 1
    if len(sys.argv) > arg_id:
        reinit_frame_skip = int(sys.argv[arg_id])
        arg_id += 1
    if len(sys.argv) > arg_id:
        reinit_err_thresh = float(sys.argv[arg_id])
        arg_id += 1
    if len(sys.argv) > arg_id:
        enable_subseq = int(sys.argv[arg_id])
        arg_id += 1
    if len(sys.argv) > arg_id:
        n_subseq = int(sys.argv[arg_id])
        arg_id += 1

    use_cle = 0
    use_opt_gt = 0

    if opt_gt_ssm == '0':
        print 'Using standard ground truth'
        use_opt_gt = 0
    else:
        use_opt_gt = 1
        print 'Using optimized ground truth with ssm: ', opt_gt_ssm
        if opt_gt_ssm == '2' or opt_gt_ssm == '2r':
            print 'Using center location error'
            use_cle = 1

    if mtf_sm is None:
        mtf_sm = mtf_sms[mtf_sm_id]
    if mtf_am is None:
        mtf_am = mtf_ams[mtf_am_id]
    if mtf_ssm is None:
        mtf_ssm = mtf_ssms[mtf_ssm_id]
    actor = actors[actor_id]
    sequences = sequences[actor]

    using_pf = False
    if mtf_sm == 'pf':
        using_pf = True

    # err_thresholds = range(1, 20)
    err_thresholds = np.linspace(1, 20, 100)
    seq_ids = range(0, len(sequences))
    # seq_ids = range(2, 3)
    n_err_thr = err_thresholds.size

    n_seq = len(seq_ids)
    # if actor_id == 0:
    # n_seq = 98

    print 'actor: {:s}'.format(actor)
    print 'mtf_sm: {:s}'.format(mtf_sm)
    print 'mtf_am: {:s}'.format(mtf_am)
    print 'mtf_ssm: {:s}'.format(mtf_ssm)
    print 'iiw: {:d}'.format(iiw)
    print 'use_opt_gt: {:d}'.format(use_opt_gt)
    print 'use_cle: {:d}'.format(use_cle)
    print 'n_seq: {:d}'.format(n_seq)

    # if os.path.isfile(out_path):
    # s = raw_input('Output file already exists:\n {:s}\n Proceed with overwrite ?\n'.format(out_path))
    # if s == 'n' or s == 'N':
    # sys.exit()

    start_ids = None
    subseq_start_ids = None
    if enable_subseq:
        subseq_file_path = '{:s}/{:s}/subseq_start_ids_{:d}.txt'.format(gt_root_dir, actor, n_subseq)
        print 'Reading sub sequence start frame ids from : {:s}'.format(subseq_file_path)
        subseq_start_ids = np.loadtxt(subseq_file_path, delimiter=',', dtype=np.uint32)
        if subseq_start_ids.shape[0] != len(sequences) or subseq_start_ids.shape[1] != n_subseq:
            raise StandardError('Sub sequence start id file has invalid data size: {:d} x {:d}'.format(
                subseq_start_ids.shape[0], subseq_start_ids.shape[1]))

    arch_fid = None
    if use_arch:
        arch_path = '{:s}/{:s}.zip'.format(arch_root_dir, arch_name)
        print 'Reading tracking data from zip archive: {:s}'.format(arch_path)
        arch_fid = zipfile.ZipFile(arch_path, 'r')

    success_rates = np.zeros((n_err_thr, n_seq + 2), dtype=np.float64)
    success_rates[:, 0] = err_thresholds
    if use_reinit_gt:
        gt_dir = '{:s}/{:s}/ReinitGT'.format(gt_root_dir, actor)
    else:
        if use_opt_gt:
            print 'Using {:s} SSM GT'.format(opt_gt_ssm)
            gt_dir = '{:s}/{:s}/OptGT'.format(gt_root_dir, actor)
        else:
            gt_dir = '{:s}/{:s}'.format(gt_root_dir, actor)
    print 'Reading GT from folder : {:s}'.format(gt_dir)

    cmb_mcd_err = []

    total_frame_count = 0
    if reinit_from_gt:
        in_arch_path = '{:s}/reinit_{:d}_{:d}'.format(in_arch_path, int(reinit_err_thresh), reinit_frame_skip)
        tracking_root_dir = '{:s}/reinit_{:d}_{:d}'.format(tracking_root_dir, int(reinit_err_thresh), reinit_frame_skip)
        out_dir = '{:s}/reinit_{:d}_{:d}'.format(out_dir, int(reinit_err_thresh), reinit_frame_skip)
        cmb_mcd_err_list = []
        cmb_failure_count = np.zeros((1, n_seq + 2), dtype=np.float64)
        frames_per_failure = np.zeros((1, n_seq + 2), dtype=np.float64)
        cmb_avg_err = np.zeros((1, n_seq + 2), dtype=np.float64)

    print 'tracking_root_dir: ', tracking_root_dir
    print 'in_arch_path: ', in_arch_path
    print 'out_dir: ', out_dir

    for j in xrange(n_seq):
        seq_id = seq_ids[j]
        seq_name = sequences[seq_id]
        # print 'seq_name: {:s}'.format(seq_name)
        if use_arch:
            tracking_res_path = '{:s}/{:s}/{:s}/{:s}_{:s}_{:s}_{:d}.txt'.format(
                in_arch_path, actor, seq_name, mtf_sm, mtf_am, mtf_ssm, iiw)
        else:
            if using_pf:
                tracking_res_path = '{:s}/pf/{:s}.txt'.format(
                    tracking_root_dir, seq_name)
            else:
                tracking_res_path = '{:s}/{:s}/{:s}/{:s}_{:s}_{:s}_{:d}.txt'.format(
                    tracking_root_dir, actor, seq_name, mtf_sm, mtf_am, mtf_ssm, iiw)
            if not os.path.isfile(tracking_res_path):
                print 'tracking result file not found for: {:s} in: \n {:s} '.format(seq_name, tracking_res_path)
                sys.exit(0)
        if use_reinit_gt:
            if use_opt_gt:
                gt_path = '{:s}/{:s}_{:s}.bin'.format(gt_dir, seq_name, opt_gt_ssm)
            else:
                gt_path = '{:s}/{:s}.bin'.format(gt_dir, seq_name)
        else:
            if use_opt_gt:
                gt_path = '{:s}/{:s}_{:s}.txt'.format(gt_dir, seq_name, opt_gt_ssm)
            else:
                gt_path = '{:s}/{:s}.txt'.format(gt_dir, seq_name)

        if enable_subseq:
            start_ids = subseq_start_ids[seq_id, :]
        mcd_err, failure_count = MCDError(tracking_res_path, gt_path, use_cle, arch_fid,
                                          reinit_from_gt, reinit_frame_skip, use_reinit_gt, start_ids)
        if mcd_err is None:
            raise StandardError('MCD Error could not be computed for sequence {:d}: {:s}'.format(j, seq_name))
        total_frame_count += len(mcd_err)
        cmb_mcd_err.append(mcd_err)
        if reinit_from_gt:
            cmb_mcd_err_list.extend(mcd_err)
            cmb_failure_count[0, j + 1] = failure_count
            frames_per_failure[0, j + 1] = float(len(mcd_err)) / float(failure_count + 1)
            if len(mcd_err) == 0:
                print 'Tracker failed in all frames so setting the average error to the threshold'
                avg_err = reinit_err_thresh
            else:
                avg_err = float(sum(mcd_err)) / float(len(mcd_err))
            cmb_avg_err[0, j + 1] = avg_err
    print 'total_frame_count: ', total_frame_count
    if reinit_from_gt:
        total_failure_count = np.sum(cmb_failure_count)
        if reinit_from_gt:
            print 'total_failure_count: {:d}'.format(int(total_failure_count))
        print 'total_error_count: ', len(cmb_mcd_err_list)
        cmb_failure_count[0, n_seq + 1] = total_failure_count
        cmb_avg_err[0, n_seq + 1] = float(sum(cmb_mcd_err_list)) / float(len(cmb_mcd_err_list))
        frames_per_failure[0, n_seq + 1] = float(total_frame_count) / float(total_failure_count + 1)
    for i in xrange(n_err_thr):
        success_frame_count = 0
        err_thr = err_thresholds[i]
        for j in xrange(n_seq):
            seq_success_count = sum(err <= err_thr for err in cmb_mcd_err[j])
            success_frame_count += seq_success_count
            if len(cmb_mcd_err[j]) == 0:
                success_rates[i, j + 1] = 0
            else:
                success_rates[i, j + 1] = float(seq_success_count) / float(len(cmb_mcd_err[j]))
        success_rates[i, n_seq + 1] = float(success_frame_count) / float(total_frame_count)
        print 'err_thr: {:15.9f} sfc: {:6d} tfc: {:6d} sr: {:15.9f}'.format(
            err_thr, success_frame_count, total_frame_count, success_rates[i, n_seq + 1])

    if not os.path.exists(out_dir):
        print 'Output directory: {:s} does not exist. Creating it...'.format(out_dir)
        os.makedirs(out_dir)

    out_path = '{:s}/sr_{:s}_{:s}_{:s}_{:s}_{:d}'.format(
        out_dir, actor, mtf_sm, mtf_am, mtf_ssm, iiw)
    if use_opt_gt:
        out_path = '{:s}_{:s}'.format(out_path, opt_gt_ssm)
    if enable_subseq:
        out_path = '{:s}_subseq_{:d}'.format(out_path, n_subseq)

    out_path = '{:s}.txt'.format(out_path)
    print 'Saving success rate data to {:s}'.format(out_path)
    if reinit_from_gt:
        out_data = np.vstack((success_rates, cmb_avg_err, cmb_failure_count, frames_per_failure))
    else:
        out_data = success_rates
    np.savetxt(out_path, out_data, fmt='%15.9f', delimiter='\t')

    if use_arch:
        arch_fid.close()
